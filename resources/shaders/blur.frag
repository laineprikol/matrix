#version 450

layout(location = 0) in vec2 vTex;

layout(location = 0) out vec4 outColor;

layout(set = 0, binding = 0) uniform sampler uSampler;
layout(set = 0, binding = 1) uniform texture2D uTexture;

layout(push_constant) uniform PushConstants {
    vec2 dir;
} uPC;

void main() {
    vec2 ts = 1.0 / textureSize(sampler2D(uTexture, uSampler), 0);

    vec4 acc = vec4(0);
    vec2 dir = uPC.dir * ts;

    vec2 off1 = vec2(1.411764705882353) * dir;
    vec2 off2 = vec2(3.294117647058823) * dir;
    vec2 off3 = vec2(5.176470588235294) * dir;

    acc += texture(sampler2D(uTexture, uSampler), vTex)        * 0.1964825501511404;
    acc += texture(sampler2D(uTexture, uSampler), vTex + off1) * 0.2969069646728344;
    acc += texture(sampler2D(uTexture, uSampler), vTex - off1) * 0.2969069646728344;
    acc += texture(sampler2D(uTexture, uSampler), vTex + off2) * 0.0944703978504473;
    acc += texture(sampler2D(uTexture, uSampler), vTex - off2) * 0.0944703978504473;
    acc += texture(sampler2D(uTexture, uSampler), vTex + off3) * 0.0103813624011480;
    acc += texture(sampler2D(uTexture, uSampler), vTex - off3) * 0.0103813624011480;

    outColor = acc;
}
