#version 450

layout(location = 0) in vec2 vUV;
layout(location = 1) in vec4 vColor;
layout(location = 2) in float vPixelRange;

layout(location = 0) out vec4 outColor;

layout(set = 0, binding = 0) uniform sampler uSampler;
layout(set = 0, binding = 1) uniform texture2D uAtlas;

float median(float r, float g, float b) {
    return max(min(r, g), min(max(r, g), b));
}

void main() {
    vec3 msd = texture(sampler2D(uAtlas, uSampler), vUV).rgb;
    float sd = median(msd.r, msd.g, msd.b);
    float screenPxDistance = vPixelRange * (sd - 0.5);
    float opacity = clamp(screenPxDistance + 0.5, 0.0, 1.0);
    outColor = mix(vec4(vColor.rgb, 0), vColor, opacity);
}
