#version 450

layout(location = 0) in vec4 inRect;
layout(location = 1) in vec4 inUVRect;
layout(location = 2) in vec4 inColor;

layout(location = 0) out vec2 vUV;
layout(location = 1) out vec4 vColor;
layout(location = 2) out float vPixelRange;

layout(push_constant) uniform PushConstants {
    mat4 proj;
} uPC;

const vec2 VERTICES[6] = {
    vec2(0, 0),
    vec2(0, 1),
    vec2(1, 0),
    vec2(1, 0),
    vec2(0, 1),
    vec2(1, 1),
};

void main() {
    vec2 unit = VERTICES[gl_VertexIndex];
    vUV = unit * inUVRect.zw + inUVRect.xy;
    vec2 pos = unit * inRect.zw + inRect.xy;
    vPixelRange = inRect.z / inUVRect.z / 512.0 * 8.0;
    vColor = inColor;
    gl_Position = uPC.proj * vec4(pos.x, pos.y, 0, 1);
}
