mod backend;
mod font;
mod renderer;

use std::collections::VecDeque;
use std::time::{Duration, Instant};

use anyhow::{anyhow, Result};
use rand::rngs::SmallRng;
use rand::{Rng, SeedableRng};
use raw_window_handle::{HasRawWindowHandle, RawWindowHandle};
use structopt::StructOpt;

use crate::backend::{enumerate_backends, BackendKind};
use crate::font::Font;
use crate::renderer::{DrawList, Encoder, Renderer};

fn smoothstep(v: f32) -> f32 {
    v * v * (3.0 - 2.0 * v)
}

struct Matrix {
    columns: Vec<VecDeque<(usize, VecDeque<u8>)>>,
    height: usize,
    cell_size: [f32; 2],
    offset: [f32; 2],
    color: [f32; 4],
    layer: usize,
}

impl Matrix {
    fn new(screen_size: [f32; 2], cell_size: [f32; 2], color: [f32; 4], layer: usize) -> Matrix {
        let num_cols = (screen_size[0] / cell_size[0]).floor() as usize;
        let col_offset = (screen_size[0] - (num_cols as f32) * cell_size[0]) * 0.5;
        let num_rows = (screen_size[1] / cell_size[1]).floor() as usize;
        let row_offset = (screen_size[1] - (num_rows as f32) * cell_size[1]) * 0.5;
        Matrix {
            columns: vec![VecDeque::new(); num_cols],
            height: num_rows,
            cell_size,
            offset: [col_offset, row_offset],
            color,
            layer,
        }
    }

    fn draw(&self, encoder: &mut Encoder<'_, '_>) {
        for (x, col) in self.columns.iter().enumerate() {
            let x = self.cell_size[0] * (x as f32) + self.offset[0];
            for (y, strip) in col {
                let mut y = self.cell_size[1] * (*y as f32) + self.offset[1];
                for (i, byte) in strip.iter().enumerate() {
                    let c = std::char::from_u32(*byte as u32 + 0x30A1).unwrap();
                    let text = format!("{}", c);
                    let k = if i == strip.len() - 1 { 2.0 } else { 1.0 };
                    let alpha = smoothstep((i as f32) / (strip.len() as f32 - 1.0));
                    let color = [
                        self.color[0] * k,
                        self.color[1] * k,
                        self.color[2] * k,
                        self.color[3] * alpha,
                    ];
                    encoder.text(self.layer, [x, y], self.cell_size[1], color, &text);
                    y += self.cell_size[1];
                }
            }
        }
    }

    fn process(&mut self, rng: &mut SmallRng) {
        for col in &mut self.columns {
            for (start_y, strip) in col.iter_mut() {
                if !(strip.len() < 15 && (strip.len() < 5 || rng.gen_bool(0.7))) {
                    *start_y += 1;
                    strip.pop_front();
                }

                strip.push_back(rng.gen_range(0..86));
            }

            while let Some((y, strip)) = col.pop_back() {
                if y < self.height {
                    col.push_back((y, strip));
                    break;
                }
            }
        }

        for col in &mut self.columns {
            let occupied = col.iter().map(|(_, s)| s.len()).sum::<usize>();
            let emptiness = 1.0 - (occupied as f64) / (self.height as f64);
            let spawn_chance = emptiness * rng.gen_range(0.0..0.2);
            if rng.gen_bool(spawn_chance.max(0.0)) {
                if col.is_empty() || col[0].0 > 5 {
                    col.push_front((0, vec![rng.gen_range(0..86)].into()));
                }
            }
        }
    }
}

fn create_matrices(width: u32, height: u32, num: usize) -> Vec<Matrix> {
    (0..num)
        .map(|layer| {
            let s = 30.0 / (layer as f32 * 0.3 + 1.0);
            let a = (1.0 - (layer as f32) / (num as f32 - 1.0)) * 0.5 + 0.5;
            Matrix::new(
                [width as f32, height as f32],
                [s, s],
                [0.05, 0.4, 0.05, a],
                layer,
            )
        })
        .collect()
}

/// A basic example
#[derive(StructOpt, Debug)]
#[structopt(name = "basic")]
struct Opt {
    /// Draw wallpaper matrix
    #[structopt(short, long)]
    wallpaper: bool,
}

struct RawWindowHandleWrapper(RawWindowHandle);

unsafe impl HasRawWindowHandle for RawWindowHandleWrapper {
    fn raw_window_handle(&self) -> RawWindowHandle {
        self.0
    }
}

fn main() -> Result<()> {
    env_logger::init();
    let opt = Opt::from_args();

    let backends = enumerate_backends();
    let backend_builder = backends
        .into_iter()
        .filter(|backend| !opt.wallpaper || backend.kind() == BackendKind::Wallpaper)
        .next()
        .ok_or_else(|| anyhow!("No suitable backends"))?;

    let mut exe_path = std::env::current_exe()?;
    exe_path.pop();
    let mut resources_path = exe_path.join("resources/");
    if !resources_path.exists() {
        resources_path = exe_path.join("../../resources/");
    }

    let mut backend = backend_builder.build()?;
    let window = RawWindowHandleWrapper(backend.get_window());

    let mut font = Font::load(
        resources_path.join("font.png"),
        resources_path.join("font.csv"),
    )?;

    let mut renderer = pollster::block_on(Renderer::new(
        &window,
        backend.get_resolution(),
        &mut font,
        &resources_path,
    ))?;

    let mut draw_list = DrawList::default();

    let mut rng = SmallRng::from_entropy();
    let mut matrices = create_matrices(800, 600, 5);
    let mut last_step = Instant::now();
    let step_time = Duration::from_millis(40);

    let mut old_resolution = [800, 600];
    backend.run_loop(Box::new(move |resolution| {
        if resolution != old_resolution {
            renderer.resize(resolution[0], resolution[1]);
            matrices = create_matrices(resolution[0], resolution[1], 5);
            old_resolution = resolution;
        }
        if last_step.elapsed() >= step_time {
            let mut encoder = draw_list.encoder(&font);

            for matrix in &mut matrices {
                matrix.process(&mut rng);
                matrix.draw(&mut encoder);
            }
            last_step = Instant::now();

            renderer.render(&draw_list)?;

            draw_list.clear();
        }

        Ok(())
    }));

    Ok(())
}
