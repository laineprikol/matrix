use std::num::NonZeroU32;
use std::path::Path;

use anyhow::Result;
use wgpu::util::{BufferInitDescriptor, DeviceExt};
use wgpu::{
    BindGroup, BindGroupLayout, BlendComponent, BlendFactor, BlendOperation, BlendState, Buffer,
    BufferUsages, ColorTargetState, ColorWrites, CommandEncoder, Device, Extent3d, FragmentState,
    ImageCopyBuffer, ImageCopyTexture, ImageDataLayout, LoadOp, Operations, Origin3d,
    PipelineLayoutDescriptor, PushConstantRange, Queue, RenderPassColorAttachment, RenderPipeline,
    RenderPipelineDescriptor, Sampler, ShaderStages, Texture, TextureAspect, TextureDescriptor,
    TextureDimension, TextureFormat, TextureUsages, TextureView, VertexAttribute,
    VertexBufferLayout, VertexFormat, VertexState, VertexStepMode,
};

use super::{create_bind_group, create_shader, view_as_bytes};
use crate::font::Font;

pub struct TextPass {
    pipeline: RenderPipeline,
    bind_group: BindGroup,
}

impl TextPass {
    pub fn new(
        device: &Device,
        queue: &Queue,
        sampler: &Sampler,
        bind_group_layout: &BindGroupLayout,
        swap_chain_format: TextureFormat,
        font: &mut Font,
        res_path: &Path,
    ) -> Result<TextPass> {
        let atlas = create_atlas(device, queue, font)?;
        let view = atlas.create_view(&Default::default());
        let bind_group = create_bind_group(&device, &bind_group_layout, &sampler, &view);
        let pipeline = create_pipeline(&device, &&bind_group_layout, swap_chain_format, res_path)?;

        Ok(TextPass {
            pipeline,
            bind_group,
        })
    }

    pub fn run(
        &self,
        device: &Device,
        encoder: &mut CommandEncoder,
        target: &TextureView,
        width: u32,
        height: u32,
        instances: &[GlyphInstance],
        clear: Option<wgpu::Color>,
    ) {
        let instance_buf = create_instance_buf(&device, instances);

        let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            label: None,
            color_attachments: &[RenderPassColorAttachment {
                view: &target,
                resolve_target: None,
                ops: Operations {
                    load: clear.map(LoadOp::Clear).unwrap_or(LoadOp::Load),
                    store: true,
                },
            }],
            depth_stencil_attachment: None,
        });

        let width = width as f32;
        let height = height as f32;
        #[rustfmt::skip]
        let matrix = [
            2.0 / width, 0.0, 0.0, 0.0,
            0.0, -2.0 / height, 0.0, 0.0,
            0.0, 0.0, 1.0, 0.0,
            -1.0, 1.0, 0.0, 1.0,
        ];

        rpass.set_pipeline(&self.pipeline);
        rpass.set_vertex_buffer(0, instance_buf.slice(..));
        rpass.set_push_constants(ShaderStages::VERTEX, 0, view_as_bytes(&matrix));
        rpass.set_bind_group(0, &self.bind_group, &[]);
        rpass.draw(0..6, 0..instances.len() as _);
    }
}

fn create_pipeline(
    device: &Device,
    bind_group_layout: &BindGroupLayout,
    swap_chain_format: TextureFormat,
    res_path: &Path,
) -> Result<RenderPipeline> {
    let vert_shader = create_shader(device, res_path.join("shaders/text.vert.spv"))?;
    let frag_shader = create_shader(device, res_path.join("shaders/text.frag.spv"))?;

    let layout = device.create_pipeline_layout(&PipelineLayoutDescriptor {
        label: None,
        bind_group_layouts: &[bind_group_layout],
        push_constant_ranges: &[PushConstantRange {
            stages: ShaderStages::VERTEX,
            range: 0..64,
        }],
    });

    Ok(device.create_render_pipeline(&RenderPipelineDescriptor {
        label: None,
        layout: Some(&layout),
        vertex: VertexState {
            module: &vert_shader,
            entry_point: "main",
            buffers: &[VertexBufferLayout {
                array_stride: 48,
                step_mode: VertexStepMode::Instance,
                attributes: &[
                    VertexAttribute {
                        format: VertexFormat::Float32x4,
                        offset: 0,
                        shader_location: 0,
                    },
                    VertexAttribute {
                        format: VertexFormat::Float32x4,
                        offset: 16,
                        shader_location: 1,
                    },
                    VertexAttribute {
                        format: VertexFormat::Float32x4,
                        offset: 32,
                        shader_location: 2,
                    },
                ],
            }],
        },
        primitive: Default::default(),
        depth_stencil: None,
        multisample: Default::default(),
        fragment: Some(FragmentState {
            module: &frag_shader,
            entry_point: "main",
            targets: &[ColorTargetState {
                format: swap_chain_format,
                blend: Some(BlendState {
                    alpha: BlendComponent {
                        src_factor: BlendFactor::One,
                        dst_factor: BlendFactor::OneMinusSrcAlpha,
                        operation: BlendOperation::Add,
                    },
                    color: BlendComponent {
                        src_factor: BlendFactor::SrcAlpha,
                        dst_factor: BlendFactor::OneMinusSrcAlpha,
                        operation: BlendOperation::Add,
                    },
                }),
                write_mask: ColorWrites::all(),
            }],
        }),
    }))
}

fn create_atlas(device: &Device, queue: &Queue, font: &mut Font) -> Result<Texture> {
    let atlas = &mut font.atlas;

    let staging_buffer = device.create_buffer_init(&BufferInitDescriptor {
        label: None,
        contents: &atlas.data,
        usage: BufferUsages::COPY_SRC,
    });

    atlas.data = Vec::new();

    let size = Extent3d {
        width: atlas.width as _,
        height: atlas.height as _,
        depth_or_array_layers: 1,
    };

    let texture = device.create_texture(&TextureDescriptor {
        label: None,
        size,
        mip_level_count: 1,
        sample_count: 1,
        dimension: TextureDimension::D2,
        format: TextureFormat::Rgba8Unorm,
        usage: TextureUsages::TEXTURE_BINDING | TextureUsages::COPY_DST,
    });

    let mut encoder = device.create_command_encoder(&Default::default());

    let src = ImageCopyBuffer {
        buffer: &staging_buffer,
        layout: ImageDataLayout {
            offset: 0,
            bytes_per_row: Some(NonZeroU32::new((atlas.width * 4) as u32).unwrap()),
            rows_per_image: Some(NonZeroU32::new(atlas.height as u32).unwrap()),
        },
    };

    let dst = ImageCopyTexture {
        texture: &texture,
        mip_level: 0,
        origin: Origin3d::ZERO,
        aspect: TextureAspect::All,
    };

    encoder.copy_buffer_to_texture(src, dst, size);
    queue.submit(Some(encoder.finish()));

    Ok(texture)
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct GlyphInstance {
    pub rect: [f32; 4],
    pub atlas_rect: [f32; 4],
    pub color: [f32; 4],
}

fn create_instance_buf(device: &Device, instances: &[GlyphInstance]) -> Buffer {
    device.create_buffer_init(&BufferInitDescriptor {
        label: None,
        contents: view_as_bytes(instances),
        usage: BufferUsages::VERTEX,
    })
}
