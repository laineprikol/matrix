mod blur_pass;
mod text_pass;

use std::fs::File;
use std::io::{Read, Seek, SeekFrom};
use std::path::Path;

use anyhow::{Context, Result};
use raw_window_handle::HasRawWindowHandle;
use wgpu::util::make_spirv;
use wgpu::{
    Adapter, Backends, BindGroup, BindGroupDescriptor, BindGroupEntry, BindGroupLayout,
    BindGroupLayoutDescriptor, BindGroupLayoutEntry, BindingResource, Device, DeviceDescriptor,
    Extent3d, Features, FilterMode, Instance, PowerPreference, PresentMode, Queue,
    RequestAdapterOptions, Sampler, SamplerDescriptor, ShaderModule, ShaderModuleDescriptor,
    ShaderStages, Surface, SurfaceConfiguration, TextureDescriptor, TextureDimension,
    TextureFormat, TextureSampleType, TextureUsages, TextureView, TextureViewDescriptor,
    TextureViewDimension,
};

use self::blur_pass::BlurPass;
use self::text_pass::{GlyphInstance, TextPass};
use crate::font::Font;

pub struct Renderer {
    device: Device,
    queue: Queue,
    surface: Surface,
    surface_config: SurfaceConfiguration,
    bind_group_layout: BindGroupLayout,
    sampler: Sampler,
    backbuffers: [TextureView; 2],
    text_pass: TextPass,
    blur_pass: BlurPass,
}

impl Renderer {
    pub async fn new<W: HasRawWindowHandle>(
        window: &W,
        resolution: [u32; 2],
        font: &mut Font,
        res_path: &Path,
    ) -> Result<Renderer> {
        let instance = Instance::new(Backends::all());
        let surface = unsafe { instance.create_surface(window) };
        let adapter = create_adapter(&instance, &surface).await?;
        let (device, queue) = create_device(&adapter).await?;

        let surface_format = surface
            .get_preferred_format(&adapter)
            .unwrap_or(TextureFormat::Bgra8UnormSrgb);

        let size_width = resolution[0].max(1);
        let size_height = resolution[1].max(1);

        let surface_config = SurfaceConfiguration {
            usage: TextureUsages::RENDER_ATTACHMENT,
            format: surface_format,
            width: size_width,
            height: size_height,
            present_mode: PresentMode::Mailbox,
        };

        surface.configure(&device, &surface_config);

        let sampler = create_sampler(&device);
        let bind_group_layout = create_bind_group_layout(&device);

        let text_pass = TextPass::new(
            &device,
            &queue,
            &sampler,
            &bind_group_layout,
            surface_format,
            font,
            res_path,
        )?;

        let blur_pass = BlurPass::new(&device, &bind_group_layout, surface_format, res_path)?;

        let backbuffer0 = create_backbuffer(&device, surface_format, size_width, size_height);
        let backbuffer1 = create_backbuffer(&device, surface_format, size_width, size_height);

        Ok(Renderer {
            device,
            queue,
            surface,
            surface_config,
            bind_group_layout,
            sampler,
            backbuffers: [backbuffer0, backbuffer1],
            text_pass,
            blur_pass,
        })
    }

    pub fn render(&mut self, list: &DrawList) -> Result<()> {
        if list.layers.is_empty() {
            return Ok(());
        }

        let frame = self.surface.get_current_frame()?;
        let frame_view = frame.output.texture.create_view(&Default::default());

        let mut encoder = self.device.create_command_encoder(&Default::default());

        for (i, layer) in list.layers.iter().enumerate().skip(1).rev() {
            let blur = (i as f32) / ((list.layers.len() - 1) as f32);

            self.text_pass.run(
                &self.device,
                &mut encoder,
                &self.backbuffers[0],
                self.surface_config.width,
                self.surface_config.height,
                &layer,
                Some(wgpu::Color::TRANSPARENT),
            );

            self.blur_pass.run(
                &self.device,
                &self.bind_group_layout,
                &self.sampler,
                &mut encoder,
                &self.backbuffers[0],
                &self.backbuffers[1],
                [blur, blur],
                Some(wgpu::Color::TRANSPARENT),
            );

            self.blur_pass.run(
                &self.device,
                &self.bind_group_layout,
                &self.sampler,
                &mut encoder,
                &self.backbuffers[1],
                &frame_view,
                [blur, -blur],
                (i == list.layers.len() - 1).then(|| wgpu::Color::BLACK),
            );
        }

        self.text_pass.run(
            &self.device,
            &mut encoder,
            &frame_view,
            self.surface_config.width,
            self.surface_config.height,
            &list.layers[0],
            (list.layers.len() == 1).then(|| wgpu::Color::BLACK),
        );

        self.queue.submit(Some(encoder.finish()));

        Ok(())
    }

    pub fn resize(&mut self, width: u32, height: u32) {
        let width = width.max(1);
        let height = height.max(1);
        self.surface_config.width = width;
        self.surface_config.height = height;
        self.surface.configure(&self.device, &self.surface_config);
        self.backbuffers[0] =
            create_backbuffer(&self.device, self.surface_config.format, width, height);
        self.backbuffers[1] =
            create_backbuffer(&self.device, self.surface_config.format, width, height);
    }
}

async fn create_adapter(instance: &Instance, surface: &Surface) -> Result<Adapter> {
    let desc = &RequestAdapterOptions {
        power_preference: PowerPreference::HighPerformance,
        compatible_surface: Some(&surface),
    };

    instance.request_adapter(&desc).await.context("No adapter")
}

async fn create_device(adapter: &Adapter) -> Result<(Device, Queue)> {
    let desc = &DeviceDescriptor {
        label: None,
        features: Features::PUSH_CONSTANTS,
        limits: wgpu::Limits {
            max_push_constant_size: 64,
            ..wgpu::Limits::default()
        },
    };

    adapter
        .request_device(&desc, None)
        .await
        .context("Cannot open device")
}

fn create_shader(device: &Device, path: impl AsRef<Path>) -> Result<ShaderModule> {
    let path = path.as_ref();
    let mut file = File::open(path).with_context(|| format!("Cannot open {}", path.display()))?;
    let size = file.seek(SeekFrom::End(0))?;
    file.seek(SeekFrom::Start(0))?;
    let mut buf = Vec::with_capacity(size as usize);
    file.read_to_end(&mut buf)?;

    Ok(device.create_shader_module(&ShaderModuleDescriptor {
        label: None,
        source: make_spirv(&buf),
    }))
}

fn create_sampler(device: &Device) -> Sampler {
    device.create_sampler(&SamplerDescriptor {
        min_filter: FilterMode::Linear,
        mag_filter: FilterMode::Linear,
        ..Default::default()
    })
}

fn create_bind_group_layout(device: &Device) -> BindGroupLayout {
    device.create_bind_group_layout(&BindGroupLayoutDescriptor {
        label: None,
        entries: &[
            BindGroupLayoutEntry {
                binding: 0,
                visibility: ShaderStages::FRAGMENT,
                ty: wgpu::BindingType::Sampler {
                    filtering: true,
                    comparison: false,
                },
                count: None,
            },
            BindGroupLayoutEntry {
                binding: 1,
                visibility: ShaderStages::FRAGMENT,
                ty: wgpu::BindingType::Texture {
                    sample_type: TextureSampleType::Float { filterable: true },
                    view_dimension: TextureViewDimension::D2,
                    multisampled: false,
                },
                count: None,
            },
        ],
    })
}

fn create_bind_group(
    device: &Device,
    layout: &BindGroupLayout,
    sampler: &Sampler,
    texture_view: &TextureView,
) -> BindGroup {
    device.create_bind_group(&BindGroupDescriptor {
        label: None,
        layout,
        entries: &[
            BindGroupEntry {
                binding: 0,
                resource: BindingResource::Sampler(sampler),
            },
            BindGroupEntry {
                binding: 1,
                resource: BindingResource::TextureView(texture_view),
            },
        ],
    })
}

fn create_backbuffer(
    device: &Device,
    surface_format: TextureFormat,
    width: u32,
    height: u32,
) -> TextureView {
    device
        .create_texture(&TextureDescriptor {
            label: None,
            size: Extent3d {
                width,
                height,
                depth_or_array_layers: 1,
            },
            mip_level_count: 1,
            sample_count: 1,
            dimension: TextureDimension::D2,
            format: surface_format,
            usage: TextureUsages::TEXTURE_BINDING | TextureUsages::RENDER_ATTACHMENT,
        })
        .create_view(&TextureViewDescriptor::default())
}

fn view_as_bytes<T>(slice: &[T]) -> &[u8] {
    let len = std::mem::size_of::<T>() * slice.len();
    unsafe { std::slice::from_raw_parts(slice.as_ptr() as *const _, len) }
}

#[derive(Default)]
pub struct DrawList {
    layers: Vec<Vec<GlyphInstance>>,
}

impl DrawList {
    pub fn encoder<'s, 'f>(&'s mut self, font: &'f Font) -> Encoder<'s, 'f> {
        Encoder { font, list: self }
    }

    pub fn clear(&mut self) {
        for layer in &mut self.layers {
            layer.clear();
        }
    }
}

pub struct Encoder<'list, 'font> {
    list: &'list mut DrawList,
    font: &'font Font,
}

impl Encoder<'_, '_> {
    pub fn text(
        &mut self,
        layer: usize,
        [mut x, y]: [f32; 2],
        size: f32,
        color: [f32; 4],
        text: &str,
    ) {
        for c in text.chars() {
            x += self.glyph(layer, [x, y], size, color, c);
        }
    }

    pub fn glyph(
        &mut self,
        layer: usize,
        [x, y]: [f32; 2],
        size: f32,
        color: [f32; 4],
        char: char,
    ) -> f32 {
        let glyphs = &self.font.layout.glyphs;
        let layout = glyphs.get(&char).or_else(|| glyphs.get(&'?')).unwrap();

        let b = layout.baseline_bounds;
        let rect = [
            b.x0 * size + x,
            y - b.y1 * size,
            (b.x1 - b.x0) * size,
            (b.y1 - b.y0) * size,
        ];

        let b = layout.atlas_bounds;
        let w = (self.font.atlas.width as f32).recip();
        let h = (self.font.atlas.height as f32).recip();
        let atlas_rect = [
            b.x0 * w,
            1.0 - b.y1 * h,
            (b.x1 - b.x0) * w,
            (b.y1 - b.y0) * h,
        ];

        let instance = GlyphInstance {
            rect,
            atlas_rect,
            color,
        };

        while self.list.layers.len() <= layer {
            self.list.layers.push(Vec::new());
        }

        let layer = &mut self.list.layers[layer];
        layer.push(instance);

        layout.advance * size
    }
}
