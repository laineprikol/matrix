use std::ffi::{c_void, CString, OsStr};
use std::iter::once;
use std::mem;
use std::os::windows::prelude::OsStrExt;
use std::ptr::null;
use std::time::Duration;

use anyhow::Result;
use raw_window_handle::windows::WindowsHandle;
use raw_window_handle::RawWindowHandle;
use winapi::shared::minwindef::{BOOL, FALSE, LPARAM, TRUE};
use winapi::shared::ntdef::NULL;
use winapi::shared::windef::{HWND, RECT};
use winapi::um::winuser::{
    self, GetSystemMetrics, SMTO_NORMAL, SM_CXVIRTUALSCREEN, SM_CYVIRTUALSCREEN,
};

use super::{Backend, BackendBuilder, BackendKind, RunCallback};

pub struct WindowsBackend {
    resolution: [u32; 2],
    handle: RawWindowHandle,
}

pub struct WindowsBackendBuilder;

impl BackendBuilder for WindowsBackendBuilder {
    fn kind(&self) -> BackendKind {
        BackendKind::Wallpaper
    }

    fn name(&self) -> &'static str {
        "windows"
    }

    fn build(&self) -> Result<Box<dyn Backend>> {
        unsafe {
            winuser::SetProcessDPIAware();
            let screen_x = GetSystemMetrics(SM_CXVIRTUALSCREEN) as u32;
            let screen_y = GetSystemMetrics(SM_CYVIRTUALSCREEN) as u32;
            let resolution = [screen_x, screen_y];
            let handle = get_wallpaper_window();
            let backend = WindowsBackend { handle, resolution };

            println!("Detected screen(s) resolution: {}x{}", screen_x, screen_y);

            Ok(Box::new(backend))
        }
    }
}

impl Backend for WindowsBackend {
    fn get_window(&mut self) -> RawWindowHandle {
        self.handle
    }

    fn get_resolution(&mut self) -> [u32; 2] {
        self.resolution
    }

    fn run_loop(self: Box<Self>, mut callback: RunCallback) {
        loop {
            // TODO: Compute proper time
            std::thread::sleep(Duration::from_millis(40));
            if !is_fullscreen(self.handle) {
                if let Err(e) = callback(self.resolution) {
                    eprintln!("Error: {:?}", e);
                    break;
                }
            }
        }
    }
}

fn is_fullscreen(desktop_handle: RawWindowHandle) -> bool {
    let mut rect_desktop: RECT = RECT {
        left: 1,
        top: 1,
        right: 1,
        bottom: 1,
    };
    let mut rect_window: RECT = RECT {
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
    };

    match desktop_handle {
        RawWindowHandle::Windows(handle) => unsafe {
            let fore = winuser::GetForegroundWindow();
            let desktop = handle.hwnd as HWND;

            let fore_class = get_window_class(fore);
            let desk_class = get_window_class(desktop);

            if fore_class != desk_class {
                winuser::GetWindowRect(desktop, &mut rect_desktop);
                winuser::GetWindowRect(fore, &mut rect_window);
            }
        },
        _ => {}
    }

    rect_window.left == rect_desktop.left
        && rect_window.top == rect_desktop.top
        && rect_window.right == rect_desktop.right
        && rect_window.bottom == rect_desktop.bottom
}

fn enumerate_windows<F>(mut callback: F)
where
    F: FnMut(HWND) -> bool,
{
    let mut trait_obj: &mut dyn FnMut(HWND) -> bool = &mut callback;
    let closure_pointer_pointer: *mut c_void = unsafe { mem::transmute(&mut trait_obj) };

    let lparam = closure_pointer_pointer as LPARAM;
    unsafe { winuser::EnumWindows(Some(enumerate_callback), lparam) };
}

fn enumerate_child_windows<F>(hwnd: HWND, mut callback: F)
where
    F: FnMut(HWND) -> bool,
{
    let mut trait_obj: &mut dyn FnMut(HWND) -> bool = &mut callback;
    let closure_pointer_pointer: *mut c_void = unsafe { mem::transmute(&mut trait_obj) };

    let lparam = closure_pointer_pointer as LPARAM;
    unsafe { winuser::EnumChildWindows(hwnd, Some(enumerate_callback), lparam) };
}

unsafe extern "system" fn enumerate_callback(hwnd: HWND, lparam: LPARAM) -> BOOL {
    let closure: &mut &mut dyn FnMut(HWND) -> bool = mem::transmute(lparam as *mut c_void);
    if closure(hwnd) {
        TRUE
    } else {
        FALSE
    }
}

fn win32_string(value: &str) -> Vec<u16> {
    OsStr::new(value).encode_wide().chain(once(0)).collect()
}

fn get_window_class<'a>(hwnd: HWND) -> String {
    let str = CString::new(vec![1; 127]).unwrap();
    unsafe {
        winuser::GetClassNameA(hwnd, str.as_ptr(), 127);
    }

    str.to_string_lossy().to_string()
}

unsafe fn get_wallpaper_window() -> RawWindowHandle {
    let name = win32_string("Progman");
    let program = winuser::FindWindowW(name.as_ptr(), null());

    winuser::SendMessageTimeoutA(program, 0x052C, 0, 0, SMTO_NORMAL, 1000, NULL as _);
    let mut wallpaper_hwnd = null();

    enumerate_windows(|hwnd_main| {
        //println!("{}", get_window_class(hwnd_main));
        enumerate_child_windows(hwnd_main as _, |_hwnd| {
            let defview = CString::new("SHELLDLL_DefView").unwrap();
            let workerw = CString::new("WorkerW").unwrap();
            //println!("- {}", get_window_class(_hwnd));

            let p =
                winuser::FindWindowExA(hwnd_main as *mut _, NULL as _, defview.as_ptr(), NULL as _);

            if !p.is_null() {
                // Gets the WorkerW Window after the current one.
                wallpaper_hwnd =
                    winuser::FindWindowExA(NULL as _, hwnd_main, workerw.as_ptr(), NULL as _);
            }
            true
        });
        true
    });

    if wallpaper_hwnd.is_null() {
        panic!("Cannot get desktop wallpaper handle!");
    }

    RawWindowHandle::Windows(WindowsHandle {
        hwnd: wallpaper_hwnd as *mut _,
        ..WindowsHandle::empty()
    })
}
