use anyhow::Result;
use raw_window_handle::{HasRawWindowHandle, RawWindowHandle};
use winit::event::{Event, WindowEvent};
use winit::event_loop::{ControlFlow, EventLoop};
use winit::window::{Window, WindowBuilder};

use super::{Backend, BackendBuilder, BackendKind, RunCallback};

pub struct WinitBackend {
    event_loop: EventLoop<()>,
    window: Window,
}

pub struct WinitBackendBuilder;

impl BackendBuilder for WinitBackendBuilder {
    fn kind(&self) -> BackendKind {
        BackendKind::Window
    }

    fn name(&self) -> &'static str {
        "winit"
    }

    fn build(&self) -> Result<Box<dyn Backend>> {
        let event_loop = EventLoop::new();
        let window = WindowBuilder::new()
            .with_title("Matrix")
            .build(&event_loop)?;

        let backend = WinitBackend { event_loop, window };
        Ok(Box::new(backend))
    }
}

impl Backend for WinitBackend {
    fn get_window(&mut self) -> RawWindowHandle {
        self.window.raw_window_handle()
    }

    fn get_resolution(&mut self) -> [u32; 2] {
        let size = self.window.inner_size();
        [size.width, size.height]
    }

    fn run_loop(self: Box<Self>, mut callback: RunCallback) {
        let mut size = self.window.inner_size();

        let lp = self.event_loop;
        let window = self.window;
        lp.run(move |event, _, control_flow| match event {
            Event::WindowEvent {
                event: WindowEvent::CloseRequested,
                ..
            } => {
                *control_flow = ControlFlow::Exit;
            }

            Event::WindowEvent {
                event: WindowEvent::Resized(new_size),
                ..
            } => {
                size = new_size;
            }

            Event::MainEventsCleared => {
                if let Err(e) = callback([size.width, size.height]) {
                    eprintln!("Error: {:?}", e);
                    *control_flow = ControlFlow::Exit;
                }
                window.request_redraw();
            }

            _ => {}
        })
    }
}
